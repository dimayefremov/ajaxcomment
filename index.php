<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<!--         <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script> -->
  <script src="//code.jquery.com/jquery.js"></script>
        <title>Title Page</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
<?php 
    require 'db.php';
    // require 'ajax.php';
    $stmt = $pdo->query('SELECT `comment`, `user_name` FROM `comments` ORDER BY `comment_id` DESC');

 ?> 
    <body>
        <form class="navbar-form" action="ajax.php" method="POST">
            <div class="wrapper">
                <label for="input-id" class="col-sm-2"><h3>Your comment:</h3></label>
                    <div class="form-group">
                    <div class="col-sm-10">
                        <textarea name="comment" id="textarea" class="form-control" rows="3" required="required"></textarea>
                        <input class="user_name" type="text" name="user_name" style="width: 200px;">
                        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                </div>
                </div>
            </div>
        </form>
        
        <div class="comments">
            <?php while($row = $stmt->fetch()):?>
                <div class="alert alert-info col-xs-7 col-sm-7 col-md-7 col-lg-7" >

                     <strong>User:</strong> <?php echo $row['user_name']; ?><br>
                     <i>Comment:</i><p><?php echo $row['comment']; ?></p>
                </div><br>

            <?php endwhile;?>
        </div>
        <style>
            .wrapper{
                display: flex;
                flex-direction: column;
                align-items: flex-start;
                /*justify-content: flex-start;*/

            }
            .comments{
             padding-left: 15px;
             min-width: 355px;    
            }
        </style>

        <script>
            function success(data) {
                $('.comments').prepend('<div class="alert alert-info col-xs-7 col-sm-7 col-md-7 col-lg-7" >   <strong>User:</strong> ' + data.user_name +'<br><i>Comment:</i><p></p>'+ data.comment+'</div><br>');
            }

            $(function(){
                $('#submit').click(function(event) {
                    event.preventDefault();
                    var text = $('#textarea').val();
                    var name = $('.user_name').val();
                    if (name && text) {
                        $.ajax({
                                url: "/ajax.php",
                                type: "POST",
                                dataType: 'json',
                                data: ({comment: text, user_name: name}),
                                success: function(data){
                                    success(data);
                                    $(".navbar-form")[0].reset();
                            }
                        });    
                    }else{
                        alert('Заполните пустое поле!!!');
                    }
                });
            });
        </script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    </body>
</html>
